extern crate gpx_parse;

pub mod ascii {
    use gpx_parse::Point;
    const ELV_GRAPH_COLUMNS: usize = 80;
    const ELV_GRAPH_ROWS: usize = 20;

    /*pub fn elv_graph_old(points: &Vec<CoordElv>) -> String {
        let pts_count = points.len();
        if pts_count == 0 {
            println!("Error, no points found in GPX track.");
            return "".to_owned();
        }

        let mut min_elv: f64 = points[0].elv;
        let mut max_elv: f64 = points[0].elv;

        for p in points {
            if p.elv < min_elv {
                min_elv = p.elv;
            }
            if p.elv > max_elv {
                max_elv = p.elv;
            }
        }

        let mut col_avg: [f64; ELV_GRAPH_COLUMNS] = [0.0; ELV_GRAPH_COLUMNS];

        for c in 0..ELV_GRAPH_COLUMNS {
            let start_pt = c * pts_count / ELV_GRAPH_COLUMNS;
            let end_pt = (c+1) * pts_count / ELV_GRAPH_COLUMNS;
            col_avg[c] = get_elv_avg_old(&points, start_pt, end_pt);
        }

        let mut graph: String = "".to_owned();

        for r in 0..ELV_GRAPH_ROWS {
            let mut line: String = "".to_owned();
            let thresh = ((max_elv - min_elv) * (ELV_GRAPH_ROWS - r) as f64)/ (ELV_GRAPH_ROWS as f64) + min_elv;
            //println!("threshold: {}", thresh);
            for c in 0..ELV_GRAPH_COLUMNS {
                if col_avg[c] > thresh {
                    line.push_str("#");
                }
                else {
                    line.push_str(" ");
                }
            }
            println!("{}", line);
            graph.push_str(&line);
            graph.push_str("\n");
        }

        let mut full_line: String = "".to_owned();
        for _ in  0..ELV_GRAPH_COLUMNS {
            full_line.push_str("#")
        }
        println!("{}", full_line);
        graph.push_str(&full_line);
        graph.push_str("\n");

        return graph;
    }*/

    pub fn elv_graph(points: &Vec<Point>) -> String {
        // Make sure we have some points in our track.
        let pts_count = points.len();
        if pts_count == 0 {
            println!("Error. No points found in GPX track.");
            return "".to_owned();
        }
        // Make sure the first point contains elevation data. If it does not we assume that none
        // of the other points have elevation data.
        if points[0].elv == None {
            println!("Error. No elevation data found in the first track point.");
            return "".to_owned();
        }

        let mut min_elv: f64 = points[0].elv.unwrap();
        let mut max_elv: f64 = points[0].elv.unwrap();

        for p in points.iter() {
            if p.elv == None {
                println!("Error. Not all track points contain elevation data.");
                return "".to_owned();
            }
            if p.elv.unwrap() < min_elv {
                min_elv = p.elv.unwrap();
            }
            if p.elv.unwrap() > max_elv {
                max_elv = p.elv.unwrap();
            }
        }

        let mut col_avg: [f64; ELV_GRAPH_COLUMNS] = [0.0; ELV_GRAPH_COLUMNS];

        for c in 0..ELV_GRAPH_COLUMNS {
            let start_pt = c * pts_count / ELV_GRAPH_COLUMNS;
            let end_pt = (c+1) * pts_count / ELV_GRAPH_COLUMNS;
            col_avg[c] = get_elv_avg(&points, start_pt, end_pt);
        }

        let mut graph: String = "".to_owned();

        for r in 0..ELV_GRAPH_ROWS {
            let mut line: String = "".to_owned();
            let thresh = ((max_elv - min_elv) * (ELV_GRAPH_ROWS - r) as f64)/ (ELV_GRAPH_ROWS as f64) + min_elv;
            //println!("threshold: {}", thresh);
            for c in 0..ELV_GRAPH_COLUMNS {
                if col_avg[c] > thresh {
                    line.push_str("#");
                }
                else {
                    line.push_str(" ");
                }
            }
            println!("{}", line);
            graph.push_str(&line);
            graph.push_str("\n");
        }

        let mut full_line: String = "".to_owned();
        for _ in  0..ELV_GRAPH_COLUMNS {
            full_line.push_str("#")
        }
        println!("{}", full_line);
        graph.push_str(&full_line);
        graph.push_str("\n");

        return graph;;
    }

    /*fn get_elv_avg_old(pts: &Vec<CoordElv>, start: usize, end: usize) -> f64 {
        if start == end {
            return pts[start].elv;
        }

        let mut avg: f64 = 0.0;

        for i in start..end {
            avg += pts[i].elv;
        }
        avg /= (end - start) as f64;
        return avg;
    }*/

    fn get_elv_avg(pts: &Vec<Point>, start: usize, end: usize) -> f64 {
        //Make sure the start point has elevation data.
        if pts[start].elv == None {
            println!("Error. No elevation data found.");
            return 0.0;
        }
        if start == end {
            return pts[start].elv.unwrap();
        }

        let mut avg: f64 = 0.0;

        for i in start..end {
            if pts[i].elv == None {
                println!("Error. No elevation data found.");
                return 0.0;
            }
            avg += pts[i].elv.unwrap();
        }
        avg /= (end - start) as f64;
        return avg;
    }
}
